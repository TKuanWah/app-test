package com.kuanwah.alpha.apptest.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.text.Editable
import android.view.View
import com.kuanwah.alpha.apptest.R
import com.kuanwah.alpha.apptest.bean.ContactBean

import kotlinx.android.synthetic.main.activity_contact_details.*
import kotlinx.android.synthetic.main.content_contact_details.*

class ContactDetailsActivity : AppCompatActivity() {
    private lateinit var contactBean: ContactBean
    private var positionIndex: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_details)

        intent.extras?.let {
            contactBean = it.getSerializable(ContactListActivity.BUNDLE_CONTACT_ITEM) as ContactBean
            positionIndex = it.getInt(ContactListActivity.BUNDLE_CONTACT_POSITION_INDEX)

            updateView()
        }
    }

    private fun updateView(){
        etFirstName.text = Editable.Factory.getInstance().newEditable(contactBean.firstName)
        etLastName.text = Editable.Factory.getInstance().newEditable(contactBean.lastName)

        contactBean.email?.let {
            etEmail.text = Editable.Factory.getInstance().newEditable(it)
        }

        contactBean.phone?.let {
            etPhone.text = Editable.Factory.getInstance().newEditable(it)
        }
    }

    fun cancel(view: View){
        val intent = Intent()
        setResult(Activity.RESULT_CANCELED, intent)
        finish()
    }

    fun save(view: View){
        contactBean.firstName = etFirstName.text.toString()
        contactBean.lastName = etLastName.text.toString()

        etEmail.text?.let {
            contactBean.email = it.toString()
        }

        etPhone.text?.let {
            contactBean.phone = it.toString()
        }

        val bundle = Bundle()
        bundle.putSerializable(ContactListActivity.BUNDLE_CONTACT_ITEM, contactBean)
        bundle.putInt(ContactListActivity.BUNDLE_CONTACT_POSITION_INDEX, positionIndex)

        val intent = Intent()
        intent.putExtras(bundle)
        setResult(Activity.RESULT_OK, intent)
        finish()
    }


}
