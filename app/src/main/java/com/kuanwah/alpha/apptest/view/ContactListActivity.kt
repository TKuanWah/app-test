package com.kuanwah.alpha.apptest.view

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.design.widget.Snackbar
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DividerItemDecoration
import android.support.v7.widget.LinearLayoutManager
import android.view.Menu
import android.view.MenuItem
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken
import com.kuanwah.alpha.apptest.R
import com.kuanwah.alpha.apptest.adapter.RvContactListAdapter
import com.kuanwah.alpha.apptest.bean.ContactBean

import kotlinx.android.synthetic.main.activity_contact_list.*
import kotlinx.android.synthetic.main.content_contact_list.*
import java.util.ArrayList

class ContactListActivity : AppCompatActivity() {
    private val gson = Gson()
    private lateinit var rvContactListAdapter: RvContactListAdapter
    private var contactBeanList: ArrayList<ContactBean> = ArrayList()

    companion object{
        const val BUNDLE_CONTACT_ITEM = "Bundle Contact Item"
        const val BUNDLE_CONTACT_POSITION_INDEX = "Bundle Contact Position Index"
        const val VIEW_CONTACT_DETAILS_REQUEST_CODE = 1000
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_contact_list)

        srlContactList.setOnRefreshListener {
            contactBeanList = retrieveJsonFromLocal()
            rvContactListAdapter.contactBeanList = contactBeanList
            rvContactListAdapter.notifyDataSetChanged()

            srlContactList.isRefreshing = false
        }

        updateView()
    }

    private fun updateView(){
        contactBeanList = retrieveJsonFromLocal()
        rvContactListAdapter = RvContactListAdapter(this@ContactListActivity, contactBeanList)

        val linearLayoutManager = LinearLayoutManager(this@ContactListActivity)
        linearLayoutManager.orientation = LinearLayoutManager.VERTICAL

        rvContactList.setHasFixedSize(true)
        rvContactList.layoutManager = linearLayoutManager
        rvContactList.adapter = rvContactListAdapter
        rvContactList.addItemDecoration(DividerItemDecoration(rvContactList.context, DividerItemDecoration.VERTICAL))
    }

    private fun retrieveJsonFromLocal(): ArrayList<ContactBean>{
        val inputStream = applicationContext.assets.open("data.json")
        val jsonString = inputStream.bufferedReader().use { it.readText() }
        val collectionType = object: TypeToken<ArrayList<ContactBean>>() {}.type

        return gson.fromJson(jsonString, collectionType)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == VIEW_CONTACT_DETAILS_REQUEST_CODE && resultCode == Activity.RESULT_OK){
            data?.let {
                val intentBundle: Bundle = it.extras
                intentBundle?.let { bundle ->
                    val positionIndex: Int = bundle.getInt(BUNDLE_CONTACT_POSITION_INDEX)
                    val contactBean = bundle.getSerializable(BUNDLE_CONTACT_ITEM) as ContactBean

                    contactBeanList.set(positionIndex, contactBean)
                    rvContactListAdapter.contactBeanList = contactBeanList
                    rvContactListAdapter.notifyDataSetChanged()

                }
            }
        }
    }
}
