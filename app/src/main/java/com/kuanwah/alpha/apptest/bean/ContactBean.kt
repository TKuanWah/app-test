package com.kuanwah.alpha.apptest.bean

import com.google.gson.annotations.SerializedName
import java.io.Serializable

data class ContactBean (
    @SerializedName("id") var id: String,
    @SerializedName("firstName") var firstName: String,
    @SerializedName("lastName") var lastName: String,
    @SerializedName("email") var email: String?,
    @SerializedName("phone") var phone: String
): Serializable