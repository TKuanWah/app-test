package com.kuanwah.alpha.apptest.adapter

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.kuanwah.alpha.apptest.R
import com.kuanwah.alpha.apptest.bean.ContactBean
import com.kuanwah.alpha.apptest.view.ContactDetailsActivity
import com.kuanwah.alpha.apptest.view.ContactListActivity
import kotlinx.android.synthetic.main.item_contact.view.*

class RvContactListAdapter(val activity: Activity, var contactBeanList: List<ContactBean>): RecyclerView.Adapter<RvContactListAdapter.ContactViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ContactViewHolder {
        val itemView = LayoutInflater.from(parent.context).inflate(R.layout.item_contact, parent, false)
        return ContactViewHolder(itemView)
    }

    override fun getItemCount(): Int = contactBeanList.size

    override fun onBindViewHolder(viewHolder: ContactViewHolder, position: Int) = viewHolder.bind(position)

    inner class ContactViewHolder(concreteView: View): RecyclerView.ViewHolder(concreteView){
        private val clContactItem = concreteView.clContactItem
        private val tvContactName = concreteView.tvContactName

        fun bind(position: Int){
            val contactBean = contactBeanList[position]

            tvContactName.text = contactBean.firstName + " " + contactBean.lastName
            clContactItem.setOnClickListener {
                val bundle = Bundle()
                bundle.putSerializable(ContactListActivity.BUNDLE_CONTACT_ITEM, contactBean)
                bundle.putInt(ContactListActivity.BUNDLE_CONTACT_POSITION_INDEX, position)

                val contactDetailsIntent = Intent(activity, ContactDetailsActivity::class.java)
                contactDetailsIntent.putExtras(bundle)
                activity.startActivityForResult(contactDetailsIntent, ContactListActivity.VIEW_CONTACT_DETAILS_REQUEST_CODE)

            }
        }
    }
}
